const TETO_INSS_2018 = 621.04;

const tabelaINSS_2018 = [
  {
    ate: 1693.72,
    aliquota: 8,
  },
  {
    ate: 2822.9,
    aliquota: 9,
  },
  {
    ate: 5645.8,
    aliquota: 11,
  },
  {
    ate: Number.MAX_SAFE_INTEGER,
    aliquota: 11,
  },
];

const tabelaIRPF_2018 = [
  {
    ate: 1903.98,
    aliquota: 0,
    deducao: 0,
  },
  {
    ate: 2826.65,
    aliquota: 7.5,
    deducao: 142.8,
  },
  {
    ate: 3751.05,
    aliquota: 15.0,
    deducao: 354.8,
  },
  {
    ate: 4664.68,
    aliquota: 22.5,
    deducao: 636.13,
  },
  {
    ate: Number.MAX_SAFE_INTEGER,
    aliquota: 27.5,
    deducao: 869.36,
  },
];

const formatarMoeda = numero => {
  const numeroFormatado = numero.toLocaleString('pt-BR', {
    style: 'currency',
    currency: 'BRL',
    maximumFractionDigits: 2,
  });

  return numeroFormatado;
};

const formatarPercentual = numero => {
  const numeroFormatado = numero.toLocaleString('pt-BR', {
    style: 'percent',
    minimumFractionDigits: 2,
  });

  return numeroFormatado;
};

export class Salario {
  /**
   *
   * @param {number} O salário bruto, que deve ser um
   * número maior ou igual a 0
   */
  constructor(pSalarioBruto) {
    /**
     * Definindo todos os atributos
     */
    this._salarioBruto = undefined;
    this._baseINSS = undefined;
    this._aliquotaINSS = undefined;
    this._descontoINSS = undefined;
    this._baseIRPF = undefined;
    this._aliquotaIRPF = undefined;
    this._descontoIRPF = undefined;
    this._salarioLiquido = undefined;

    this._validarSalarioBruto(pSalarioBruto);
    this._realizarCalculos();

    /**
     * Congelando (bom pra imutabilidade)
     */
    Object.freeze(this);
  }

  _validarSalarioBruto(pSalarioBruto) {
    /**
     * Regras de validação
     */
    if (
      pSalarioBruto === undefined ||
      typeof pSalarioBruto !== 'number' ||
      pSalarioBruto < 0
    ) {
      throw new Error(
        'O parâmetro do salário bruto ' +
          'é obrigatório e deve ser um ' +
          'valor do tipo number maior ou igual a 0!',
      );
    }

    //Salário bruto validado!
    this._salarioBruto = pSalarioBruto;
  }

  _realizarCalculos() {
    this._baseINSS = this._salarioBruto;
    this._calcularDescontoINSS();
    this._baseIRPF = this._salarioBruto - this._descontoINSS;
    this._calcularDescontoIRPF();
    this._salarioLiquido =
      this._salarioBruto - this._descontoINSS - this._descontoIRPF;
  }

  _calcularDescontoINSS() {
    let descontoINSS = 0;

    /**
     * Pra cada item da tabelaINSS, procuramos
     * onde a baseINSS "se encaixa". Assim que
     * acharmos, calculamos o desconto e
     * "quebramos" o loop para evitar cálculos
     * desnecessários
     */
    for (let item of tabelaINSS_2018) {
      if (this._baseINSS <= item.ate) {
        /**
         * Nesse momento, encontramos o item correto.
         * Precisamos calcular o desconto com base no teto do INSS.
         * Por isso, o Math.min auxilia garantindo o valor
         * mínimo com base em TETO_INSS_2018
         */
        descontoINSS = Math.min(
          this._baseINSS * (item.aliquota / 100),
          TETO_INSS_2018,
        );

        /**
         * Definindo os atributos referentes ao INSS
         */
        this._aliquotaINSS = item.aliquota;
        this._descontoINSS = descontoINSS;

        /**
         * Uma vez que encontramos os valores, forçamos o
         * encerramento do for para evitar
         * calculos desnecessários (performance)
         */
        return;
      }
    }
  }

  /**
   * O cálculo é bastante semelhante ao do INSS,
   * exceto pelo teto, que não existe e pela aplicação
   * da dedução
   */
  _calcularDescontoIRPF() {
    let descontoIRPF = 0;

    for (let item of tabelaIRPF_2018) {
      if (this._baseIRPF <= item.ate) {
        descontoIRPF = this._baseIRPF * (item.aliquota / 100);
        //Aplicando a dedução
        descontoIRPF -= item.deducao;

        /**
         * Definindo os atributos referentes ao INSS
         */
        this._aliquotaIRPF = item.aliquota;
        this._descontoIRPF = descontoIRPF;

        /**
         * Encerrando o método, uma vez que
         * encontramos os valores e efetuamos
         * os cálculos
         */
        return;
      }
    }

    return descontoIRPF;
  }

  get salarioBruto() {
    return Number(this._salarioBruto.toFixed(2));
  }

  get salarioBrutoFormatado() {
    return formatarMoeda(this.salarioBruto);
  }

  get baseINSS() {
    return Number(this._baseINSS.toFixed(2));
  }

  get baseINSSFormatada() {
    return formatarMoeda(this.baseINSS);
  }

  get aliquotaINSS() {
    return Number((this._aliquotaINSS / 100).toFixed(2));
  }

  get aliquotaINSSFormatada() {
    return formatarPercentual(this.aliquotaINSS);
  }

  get descontoINSS() {
    return Number(this._descontoINSS.toFixed(2));
  }

  get isTetoINSS() {
    return this.descontoINSS === TETO_INSS_2018;
  }

  get descontoINSSFormatado() {
    return formatarMoeda(this.descontoINSS);
  }

  get baseIRPF() {
    return Number(this._baseIRPF.toFixed(2));
  }

  get baseIRPFFormatada() {
    return formatarMoeda(this.baseIRPF);
  }

  get aliquotaIRPF() {
    return Number((this._aliquotaIRPF / 100).toFixed(3));
  }

  get aliquotaIRPFFormatada() {
    return formatarPercentual(this.aliquotaIRPF);
  }

  get descontoIRPF() {
    return Number(this._descontoIRPF.toFixed(2));
  }

  get descontoIRPFFormatado() {
    return formatarMoeda(this.descontoIRPF);
  }

  get totalDescontos() {
    return Number((this._descontoINSS + this._descontoIRPF).toFixed(2));
  }

  get totalDescontosFormatado() {
    return formatarMoeda(this.totalDescontos);
  }

  get salarioLiquido() {
    return Number(this._salarioLiquido.toFixed(2));
  }

  get salarioLiquidoFormatado() {
    return formatarMoeda(this.salarioLiquido);
  }

  get percentualDescontos() {
    return this.totalDescontos / this.salarioBruto || 0.0;
  }

  get percentualDescontosFormatado() {
    return formatarPercentual(this.percentualDescontos);
  }
}

import React, { Component } from 'react';
import './css/SalarioApp.css';

import { MyInput } from './components/MyInput.js';
import { Salario } from './models/Salario.js';
import * as mockData from './assets/mock.json';

const MAX_NUMBER = 1000000;

export class SalarioApp extends Component {
  constructor() {
    super();

    this.state = {
      /**
       * Estado inicial para o cargo
       */
      cargo: {
        nome: 'Programador',
        mensagemErro: '',
      },

      /**
       * Estado inicial para a empresa
       */
      empresa: {
        nome: 'XPTO',
        mensagemErro: '',
      },

      /**
       * Estado inicial para o salário bruto
       */
      salarioBruto: {
        valor: 3000,
        mensagemErro: '',
      },

      /**
       * Lista de salários
       */
      listaSalarios: [],

      /**
       * Objeto de salário, que calcula
       * todos os outros valores importantes.
       * Inicializando com 1000 para já mostrar
       * valores no início.
       */
      salarioObj: new Salario(1000),

      /**
       * Item selecionado na tabela
       */
      itemSelecionado: -1,
    };
  }

  _validarCadastrarSalario = () => {
    if (
      this.state.salarioBruto === 0 ||
      this.state.salarioBruto >= MAX_NUMBER
    ) {
      const novoSalarioBruto = this.state.salarioBruto;
      novoSalarioBruto.erro();
      this.setState({ salarioBruto: novoSalarioBruto });
      return;
    }

    const newLista = this.state.listaSalarios;
    newLista.push({
      cargo: this.state.cargo.nome,
      empresa: this.state.empresa.nome,
      salario: this.state.salarioObj,
    });

    this.setState({ listaSalarios: newLista });
  };

  _importarDadosAleatorios = () => {
    /**
     * Obtendo 20 números aleatórios para
     * escolher os 20 itens que aparecerão
     * na lista
     */
    let i = 1;
    const numeros = [];
    const importData = [];

    while (i < 20) {
      const novoNumero = Math.floor(Math.max(0, Math.random() * 1000 - 1));
      if (numeros.indexOf(novoNumero) === -1) {
        numeros.push(novoNumero);
        importData.push({
          cargo: mockData[novoNumero].cargo,
          empresa: mockData[novoNumero].empresa,
          salario: new Salario(mockData[novoNumero].salarioBruto),
        });
        i++;
      }
    }

    this.setState({ listaSalarios: importData });
  };

  _validarEmpresa = event => {
    const newEmpresa = this.state.empresa;
    newEmpresa.nome = event.target.value.trim();

    /**
     * Verificando se a empresa é válida
     */
    newEmpresa.mensagemErro = !!newEmpresa.nome
      ? ''
      : 'A empresa é obrigatória!';

    this.setState({ empresa: newEmpresa });
  };

  _validarCargo = event => {
    const newCargo = this.state.cargo;
    newCargo.nome = event.target.value.trim();

    /**
     * Verificando se o cargo é válido
     */
    newCargo.mensagemErro = !!newCargo.nome ? '' : 'O cargo é obrigatório!';

    this.setState({ cargo: newCargo });
  };

  _validarSalarioBruto = event => {
    const novoSalarioBruto = this.state.salarioBruto;
    novoSalarioBruto.valor = Number(event.target.value);

    /**
     * Verificando se o salário bruto é válido
     */
    novoSalarioBruto.mensagemErro =
      !!novoSalarioBruto.valor > 0 ? '' : 'Salário inválido!';

    const salarioObj = new Salario(novoSalarioBruto.valor);

    this.setState({
      salarioBruto: novoSalarioBruto,
      /**
       * Aqui, garantimos a imutabilidade,
       * pois um novo objeto é vinculado
       * ao state
       */
      salarioObj: salarioObj,
    });
  };

  _limparDados = () => {
    this.setState({ listaSalarios: [] });
  };

  _deveDesabilitarCadastro = () =>
    !!this.state.cargo.mensagemErro ||
    !!this.state.empresa.mensagemErro ||
    !!this.state.salarioBruto.mensagemErro;

  _selectItem = (item, index) => {
    this.setState({
      cargo: {
        nome: item.cargo,
        mensagemErro: '',
      },

      empresa: {
        nome: item.empresa,
        mensagemErro: '',
      },

      salarioBruto: {
        valor: item.salario.salarioBruto,
        mensagemErro: '',
      },

      salarioObj: new Salario(item.salario.salarioBruto),
      itemSelecionado: index,
    });
  };

  _removeItem = indexItem => {
    /**
     * Obtendo lista atual
     */
    const newLista = this.state.listaSalarios;

    /**
     * Removendo o elemento escolhido
     */
    newLista.splice(indexItem, 1);

    /**
     * Redefinindo a lista (imutabilidade)
     */
    this.setState({ listaSalarios: newLista });
  };

  render() {
    return (
      <div>
        <header>
          <h1>Calculador de salários com React</h1>
        </header>

        <h3>Preencha o cadastro e confira os cálculos automáticos.</h3>

        <div className="formulario">
          <form>
            <MyInput
              autoFocus
              id="cargo"
              descricao="Cargo:"
              placeholder="Informe o cargo"
              editavel="true"
              onChange={event => this._validarCargo(event)}
              value={this.state.cargo.nome}
              valorInvalido={this.state.cargo.mensagemErro}
            />

            <MyInput
              id="empresa"
              descricao="Empresa:"
              placeholder="Informe a empresa"
              editavel="true"
              onChange={event => this._validarEmpresa(event)}
              value={this.state.empresa.nome}
              valorInvalido={this.state.empresa.mensagemErro}
            />

            <MyInput
              id="salarioBruto"
              descricao="Salário bruto:"
              placeholder="Informe o salário bruto"
              editavel="true"
              onChange={event => this._validarSalarioBruto(event)}
              value={this.state.salarioBruto.valor}
              type="number"
              valorInvalido={this.state.salarioBruto.mensagemErro}
            />

            <MyInput
              id="baseINSS"
              descricao="Base INSS:"
              value={this.state.salarioObj.baseINSSFormatada}
            />

            <MyInput
              id="aliquotaINSS"
              descricao="Alíquota INSS:"
              value={this.state.salarioObj.aliquotaINSSFormatada}
            />

            <MyInput
              id="descontoINSS"
              descricao="Desconto INSS:"
              value={`${this.state.salarioObj.descontoINSSFormatado} ${
                this.state.salarioObj.isTetoINSS ? '(TETO)' : ''
              }`}
            />

            <MyInput
              id="baseIRPF"
              descricao="Base IRPF:"
              value={this.state.salarioObj.baseIRPFFormatada}
            />

            <MyInput
              id="aliquotaIRPF"
              descricao="Alíquota IRPF:"
              value={this.state.salarioObj.aliquotaIRPFFormatada}
            />

            <MyInput
              id="descontoIRPF"
              descricao="Desconto IRPF:"
              value={this.state.salarioObj.descontoIRPFFormatado}
            />

            <MyInput
              id="salarioLiquido"
              descricao="Salário líquido:"
              value={this.state.salarioObj.salarioLiquidoFormatado}
            />

            <input
              id="btCadastrar"
              type="button"
              value="Adicionar"
              disabled={this._deveDesabilitarCadastro()}
              onClick={() => this._validarCadastrarSalario()}
            />

            <input
              id="btDadosAleatorios"
              type="button"
              value="Importar dados aleatórios"
              onClick={() => this._importarDadosAleatorios()}
            />

            <input
              id="btLimpar"
              type="button"
              value="Limpar"
              onClick={() => this._limparDados()}
            />
          </form>
        </div>

        <div className="listaSalarios">
          <h3>Cadastro de salários</h3>
          {this.state.listaSalarios.length > 0 ? (
            <table>
              <thead>
                <tr>
                  <th>Cargo</th>
                  <th>Empresa</th>
                  <th>Bruto</th>
                  <th>Líquido</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                {this.state.listaSalarios.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      className={
                        index === this.state.itemSelecionado
                          ? 'itemSelecionado'
                          : null
                      }
                      onClick={() => this._selectItem(item, index)}>
                      <td>{item.cargo}</td>
                      <td>{item.empresa}</td>
                      <td>{item.salario.salarioBrutoFormatado}</td>
                      <td>{item.salario.salarioLiquidoFormatado}</td>
                      <td style={{ textAlign: 'center' }}>
                        <button
                          className="x"
                          onClick={() => this._removeItem(index)}>
                          x
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          ) : (
            <h4>Nenhum salário cadastrado até o momento</h4>
          )}
        </div>
      </div>
    );
  }
}

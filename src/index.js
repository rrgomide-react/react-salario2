import React from 'react';
import ReactDOM from 'react-dom';
import { SalarioApp } from './SalarioApp';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<SalarioApp />, document.getElementById('root'));
registerServiceWorker();

import React from 'react';
import './MyInput.css';

export const MyInput = props => {
  return (
    <div className="divInput">
      <label htmlFor={props.id}>{props.descricao}</label>
      <input
        autoFocus={props.autoFocus}
        className={props.editavel ? 'inputEditavel' : ''}
        type={props.type || 'text'}
        id={props.id}
        placeholder={props.placeholder}
        onChange={props.onChange}
        disabled={!props.editavel}
        value={props.value}
        step={10}
        min={0}
        max={999999}
        onInput={props.onInput}
      />
      <span style={{ display: props.valorInvalido ? 'block' : 'none' }}>
        {props.valorInvalido}
      </span>
    </div>
  );
};
